fn main() {
    println!("Hello, world!");

    // scope
    {
        let s = "hello";
    }
    let mut s = String::from("hello");
    s.push_str(", world!");
    println!("s={}", s);

    // move
    let x = 5;
    let y = x;
    println!("x={}", x);
    println!("y={}", y);
    let x = 5;
    let mut y = x;
    // x = x + 1;
    y = y + 1;
    println!("x={}", x);
    println!("y={}", y);

    let s1 = String::from("hello");
    let s2 = s1;
    // println!("s1={}", s1); // error
    println!("s2={}", s2);

    let s1 = String::from("hello");
    let s2 = s1.clone();
    println!("s1={}", s1);
    println!("s2={}", s2);

    // 可Copy
    // 所有整数类型，比如 u32。
    // 布尔类型，bool，它的值是 true 和 false。
    // 所有浮点数类型，比如 f64。
    // 字符类型，char。
    // 元组，当且仅当其包含的类型也都是 Copy 的时候。比如，(i32, i32) 是 Copy 的，但 (i32, String) 就不是。

    let x = 5;
    copy_test_i32(x);
    println!("x={}", x);

    let s = String::from("hello");
    copy_test_string(s);
    // println!("s={}", s); // error

    // borrowing
    let s = String::from("hello");
    caculate_length(&s);
    println!("s={}", s);

    let mut s = String::from("hello");
    append_world(&mut s);
    println!("s={}", s);

    let s = String::from("hello");
    let slice = &s[3..];
    println!("slice={}", slice);
    println!("s={}", s);
}

fn copy_test_i32(x: i32) {
    println!("x={}", x);
}

fn copy_test_string(s: String) {
    println!("s={}", s);
}

fn caculate_length(s: &String) {
    let length = s.len();
    println!("{}'s length is {}", s, length);
}

fn append_world(s: &mut String) {
    s.push_str(", world!");
}
