extern crate rand;

use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("猜数字");

    let secret_number = rand::thread_rng().gen_range(1, 101);
    // println!("秘密数字是：{}", secret_number);

    println!("请输入数字：");
    loop {
        let mut guess = String::new();

        io::stdin().read_line(&mut guess).expect("读取错误");

        // let guess: u32 = guess.trim().parse().expect("Please type a number!");
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("请输入数字！");
                continue;
            }
        };

        println!("你猜的数字为: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
