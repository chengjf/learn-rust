const MAX_STEPS: u32 = 100000;
fn main() {

    println!("The const value of MAX_STEPS is: {}.", MAX_STEPS);
    const MAX_POINTS: u32 = 100000;
    println!("The const value of MAX_POINTS is: {}.", MAX_POINTS);
    let mut x = 5;
    println!("The value of x is: {}.", x);
    x = 6;
    println!("The value of x is: {}.", x);

    let x = 6;
    println!("The value of x is: {}.", x);
    let x = 4+1;
    println!("The value of x is: {}.", x);
    let x = "123你好";
    println!("The value of x is: {}.", x);


    let guess: u32 = "42".parse().expect("Not a number!");
    println!("The value of guess is: {}", guess);

   

}
