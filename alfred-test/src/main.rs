extern crate base64;
extern crate url;

// use base64::{decode, encode};
use std::env;
use std::io;
use url::percent_encoding::{percent_decode, utf8_percent_encode, DEFAULT_ENCODE_SET};

fn main() {
    // println!("Hello, world!");
    let args: Vec<String> = env::args().collect();
    let query = &args[1];
    let result = base64::encode(query);
    // println!("{}", result);

    let iter = utf8_percent_encode(query, DEFAULT_ENCODE_SET);
    let encoded: String = iter.collect();
    // println!("{}", encoded);

    alfred::json::write_items(
        io::stdout(),
        &[
            alfred::Item::new(&result),
            alfred::ItemBuilder::new(&encoded)
                .subtitle("Subtitle")
                .into_item(),
            alfred::ItemBuilder::new(&encoded)
                .arg("Argument")
                .subtitle("Subtitle")
                .icon_filetype("public.folder")
                .into_item(),
        ],
    );
}
