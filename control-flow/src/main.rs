fn main() {
    println!("Hello, world!");
    let number = 7;

    if number < 5 {
        println!("condition was true");
    } else {
        println!("condition was false");
    }

    let number = 7;

    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }

    let condition = true;
    let number = if condition { 5 } else { 6 };

    println!("The value of number is: {}", number);

    // loop {
    //     println!("again!");
    // }

    let mut i = 0;
    while i < 10 {
        println!("i={}", i);
        i = i + 1;
    }

    let a = [10, 20, 30, 40, 50];

    for element in a.iter() {
        println!("the value is: {}", element);
    }

    for (index, element) in a.iter().enumerate() {
        println!("the {} value is: {}", index, element);
    }

    for number in 10..20 {
        println!("the number is: {}", number);
    }

    for number in (10..20).rev() {
        println!("the number is: {}", number);
    }
}
