fn main() {
    println!("Hello, world!");

    let encoder = Encoder::new();
    let a = "abcdefghijklmnopqrstuvwxyz01234567890我们";
    // let a = "s131";
    let input:&[u8] = a.as_bytes();
    let result = encoder.encode(&input.to_vec());
    println!("{}", result);

    let decoder = Decoder::new();
    let result = decoder.decode(&result.as_bytes().to_vec());
    println!("{}", result);

}

struct Decoder{

}

struct Encoder{

}

impl Decoder{
    fn new() -> Decoder{
        Decoder{}
    }

    fn decode(&self, input:&Vec<u8>) -> String {
        let mut output: Vec<char> = Vec::new();
        let length = input.len();
        let mut i = 0;
        let mut eq = 0;
        while i < length {
            // i = i + 1;
            // let index = ENCODE.iter().position(|&x|x==c);
            // println!("index:{:?}", index);
            // match index {
            //     Some(index) => {

            //     },
            //     None => {
            //       match c {
            //           '='=> {
            //               index = 0;
            //           }
            //       }      
            //     }
            // };

            // 4->3
            let c1 = input[i] as char;
            i = i + 1;
            let c2 = input[i] as char;
            i = i + 1;
            let c3 = input[i] as char;
            i = i + 1;
            let c4 = input[i] as char;
            i = i + 1;

            println!("c1={:?} c2={:?} c3={:?} c4={:?}", c1, c2, c3, c4);
            let c1_index = ENCODE.iter().position(|&x|x==c1);
            let c2_index = ENCODE.iter().position(|&x|x==c2);
            let c3_index = ENCODE.iter().position(|&x|x==c3);
            let c4_index = ENCODE.iter().position(|&x|x==c4);

            println!("c1={:?} c2={:?} c3={:?} c4={:?}", c1_index, c2_index, c3_index, c4_index);
            let mut c = 0;
            if c1_index.is_some(){
                c = c | (c1_index.unwrap() << 18);
                println!("c1 {:?}", c);
            }
            if c2_index.is_some(){
                c = c | (c2_index.unwrap() << 12);
                println!("c2 {:?}", c);
            }
            if c3_index.is_some(){
                c = c | (c3_index.unwrap() << 6);
                println!("c3 {:?}", c);
            }
            if c4_index.is_some(){
                c = c | c4_index.unwrap();
                println!("c4 {:?}", c);
            }

            
            c = c & 0xffffff;
            let r1 = ((c & 0xff0000) >> 16) as u8 as char;
            let r2 = ((c & 0x00ff00) >> 8) as u8 as char;
            let r3 = (c & 0x0000ff) as u8 as char;
            output.push(r1);
            output.push(r2);
            output.push(r3);

            
            println!("c={} r1={} r2={} r3={}", c, r1, r2, r3);
        }
        let result = output.into_iter().collect();
        return result;
    }
}


impl Encoder{
    fn new()->Encoder{
         Encoder{}
    }

    fn encode(&self, input:&Vec<u8>)->String{
        let mut output: Vec<char> = Vec::new();
        let length = input.len();
        for i in (0..length).step_by(3){
            let mut c = (input[i] as u32) << 16;
            let mut appendCount = 0;
            if length - i == 1 {
                appendCount = 2;
            }else if length - i == 2 {
                c = c | (input[i+1] as u32) << 8;
                appendCount = 1;
            }else {
                c = c | (input[i+1] as u32) << 8;
                c = c | (input[i+2] as u32);
                appendCount = 0;
            }
            println!("{} {} {} {} ", (input[i] as u32), c, c>>18, c>>18&0x3f);
            output.push(ENCODE[(c>>18&0x3f) as usize]);
            output.push(ENCODE[(c>>12&0x3f) as usize]);
            if appendCount == 2 {
                output.push('=');
                output.push('=');
            }else if appendCount == 1{
                output.push(ENCODE[(c>>6&0x3f) as usize]);
                output.push('=');
            }else if appendCount == 0{
                output.push(ENCODE[(c>>6&0x3f) as usize]);
                output.push(ENCODE[(c&0x3f) as usize]);
            }
            
        }
        let result = output.into_iter().collect();
        return result;
    }
}

    const ENCODE: &[char] = &[
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/',
    ];
    const ENCODE_WEBSAFE: &'static [char] = &[
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_',         
    ];

    const DECODE: &'static [i32] = &[
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1,
            -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
            -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    ];
    const DECODE_WEBSAFE: &'static [i32] = &[
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1,
                52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1,
                -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
                15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63,
                -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    ];
