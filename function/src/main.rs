fn main() {
    println!("Hello, world!");
    another_function();
    another_function_with_parameter(32);
    another_function_with_two_parameter(32, 3.1415926);
    println!("another_function_with_return:{}", another_function_with_return());
}

fn another_function() {
    println!("Another function.");
}

fn another_function_with_parameter(x: i32) {
    println!("Another function. x={}", x);
}

fn another_function_with_two_parameter(x: i32, y: f32) {
    println!("Another function. x={}, y={}", x, y);
}

fn another_function_with_return() -> i32 {
    10
}