fn main() {
     // integer
    let decimal = 98_22;
    println!("The value of decimal is: {}.", decimal);
    let hex = 0xff;
    println!("The value of hex is: {}.", hex);
    let octal = 0o77;
    println!("The value of octal is: {}.", octal);
    let binary = 0b1111_1111;
    println!("The value of binary is: {}.", binary);
    let byte = b'A';
    println!("The value of byte is: {}.", byte);

    // float
    let f64 = 2.12345;
    println!("The value of f64 is: {}.", f64);
    let f32 = 2.123412312;
    println!("The value of f32 is: {}.", f32);

     // 加法
    let sum = 5 + 10;
    println!("The value of sum is: {}.", sum);
    // 减法
    let difference = 95.5 - 4.3;
    println!("The value of difference is: {}.", difference);
    // 乘法
    let product = 4 * 30;
    println!("The value of product is: {}.", product);
    // 除法
    let quotient = 56.7 / 32.2;
    println!("The value of quotient is: {}.", quotient);
    // 取余
    let remainder = 43 % 5;
    println!("The value of remainder is: {}.", remainder);


    // bool
    let t = true;
    println!("The value of t is: {}.", t);
    let f: bool = false; // 显式指定类型注解
    println!("The value of f is: {}.", f);

    // char
    let c = 'z';
    println!("The value of c is: {}.", c);
    let z = 'ℤ';
    println!("The value of z is: {}.", z);
    let heart_eyed_cat = '😻';
    println!("The value of heart_eyed_cat is: {}.", heart_eyed_cat);


    // tuple
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    // println!("The value of tup is: {}.", tup);
    let (x, y, z) = tup;
    println!("The value of x,y,z is: {},{},{}.", x, y, z);
    let five_hundred = tup.0;
    let six_point_four = tup.1;
    let one = tup.2;
    println!("The value of five_hundred,six_point_four,one is {},{},{}.", five_hundred, six_point_four, one);

    let a = [1, 2, 3, 4, 5];
    // println!("The value of a is: {}", a);
    println!("The value of a is {},{},{},{},{}.", a[0], a[1], a[2], a[3], a[4]);
}
