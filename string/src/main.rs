fn main() {
    println!("Hello, world!");

    let mut s = String::from("foo");
    s.push_str("bar");
    s.push('x');
    println!("s={}", s);

    let s2 = "123";

    let s3 = s + s2;
    // s had been moved
    // println!("s={} s2={} s3={}", s, s2, s3);

    let x1 = String::from("tic");    
    let x2 = String::from("tac");
    let x3 = String::from("toe");
     let x = x1 + &x2 + &x3;
     // x1 had been moved
     println!("x1={} x2={} x3={} x={}", x2, x2, x3, x);

    let x1 = String::from("tic");    
    let x2 = String::from("tac");
    let x3 = String::from("toe");
    let x = format!("{}-{}-{}", x1, x2, x3);
    println!("x1={} x2={} x3={} x={}", x1, x2, x3, x);

    // length
    let x = "hello";
    let len = x.len();
    println!("x[{}]'s length is {}", x, len);
    let x = "你好";
    let len = x.len();
    println!("x[{}]'s length is {}", x, len);

    for c in x.chars() {
        println!("{}", c);
    }

    for b in x.bytes(){
        println!("{}", b);
    }
}
