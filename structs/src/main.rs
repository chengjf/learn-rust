fn main() {
    println!("Hello, world!");
    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };
    print_user(&user1);

    let user = build_user(String::from("a@b.com"), String::from("Bob"));
    print_user(&user);

    let user2 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        ..user
    };
    print_user(&user2);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0);
    println!("{},{},{}", black.0, black.1, black.2);
    println!("{},{}", origin.0, origin.1);
}

fn print_user(user: &User) {
    println!(
        "User[username:{}, email:{}, sign_in_count:{}, active:{}]",
        user.username, user.email, user.sign_in_count, user.active
    );
}

struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 233,
    }
}

struct Color(i32, i32, i32);
struct Point(i32, i32);
